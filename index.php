
<?php

$result = "";
$data = array();
$id = array();
$salary= array();

$file = fopen('salaryinfo.txt', 'r');

while (!feof($file)){
    $line = fgets($file);

    $salaryId = explode(" ", $line);
    $id[]     = $salaryId[0];
    $salary[] = $salaryId[1];

    $data = array_combine($id, $salary);
}

fclose($file);

$average = array_sum($salary) / count($salary);
$Maxsalary = max($salary);
$MinSalary = min($salary);

if(isset($_POST["employee-id"])){

    $eID = $_POST["employee-id"];

    foreach ($data as $key => $value) {
        if ($eID == $key) {
            $result = $value;
        }
    }
}

if($result == "" ){
    $result = "ID not found.";
}
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Employer Salary</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="salary-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6" style="margin-top: 75px;">
                    <table class="table" border="2">
                        <tbody>
                            <tr>
                                <td>Average Salary Amount : </td>
                                <td><?php echo $average . " Tk"; ?></td>
                            </tr>

                            <tr>
                                <td>Maximum Salary Amount : </td>
                                <td> <?php echo $Maxsalary . "Tk"; ?> </td>
                            </tr>
                            <tr>
                                <td>Minimum Salary Amount : </td>
                                <td> <?php echo $MinSalary . "Tk"; ?> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col col-lg-6" style="margin-top: 75px;">
                    <div class="user-form" >
                        <form action="index.php" method="post" >
                            <div class="form-group">
                                <label for="emp_id">Enter Employee ID </label>
                                <input type="text" name="employee-id" class="form-control" id="emp_id" >
                            </div>
                            <button type="submit" class="btn btn-primary">Show Salary</button>

                        </form>
                        <br>
                        <div>
                            <p> Salary Amount: <?php echo $result ."Tk";?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>